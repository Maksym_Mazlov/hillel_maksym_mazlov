package golovach.javaCore_1.loop;

public class App00 {
    public static void main(String[] args) {
        int[] arr = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100};

        for (int k = arr.length - 1; k >= 0; k--) {  //печать в обратном порядке
            System.out.println(k + " ---- " + arr[k]);
        }
    }
}
