package golovach.javaCore_1.loop;

public class App01 {
    public static void main(String[] args) {
        int a = 2;
        int b = 10;
        System.out.println(a + " " + b);
        int tmp;

        tmp = a;        //
        a = b;          // поменяли местами значеня через доп. переменную.
        b = tmp;        //

        System.out.println(a + " " + b);

        int c = 5;
        int d = 15;
        System.out.println(c + " " + d);

        c = c + d;      //
        d = c - d;      // поменяли местами значения без доп.переменной. только для int
        c = c - d;      //

        System.out.println(c + " " + d);
    }
}
