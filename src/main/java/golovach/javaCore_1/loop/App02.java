package golovach.javaCore_1.loop;

import java.util.Arrays;

public class App02 {
    public static void main(String[] args) {

        int[] arr = {10, 20, 30, 40, 50, 60, 70, 80, 90};
        System.out.println(Arrays.toString(arr));

        int length = arr.length;
        for (int k = 0; k < length / 2; k++) {  // k < length / 2 - середина массива
            int tmp = arr[k];                   // сохраняем в tmp значение в ячейки arr[ k ] (от нуля к середине
            arr[k] = arr[length - 1 - k];       // arr[length - 1 - k] - последний елемент - к  (меняем последний на первый)
            arr[length - 1 - k] = tmp;          // в последнюю ячейку записываем первую
        }
        System.out.print(Arrays.toString(arr));

    }
}
