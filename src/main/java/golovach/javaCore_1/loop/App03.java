package golovach.javaCore_1.loop;

public class App03 {
    public static void main(String[] args) {

    }

    public static int[] merge(int[] left, int[] right) {
        int[] result = new int[left.length + right.length];
        int leftIndex = 0;
        int rightIndex = 0;
        while (leftIndex + rightIndex < result.length) {
            if (left[leftIndex] < right[rightIndex]) {
                result[leftIndex + rightIndex] = left[leftIndex++];
            } else {
                result[leftIndex + rightIndex] = right[rightIndex++];
            }
        }
        return result;
    }
}
