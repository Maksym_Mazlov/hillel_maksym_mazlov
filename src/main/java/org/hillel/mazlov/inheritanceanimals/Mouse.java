package org.hillel.mazlov.inheritanceanimals;

/**
 * class Mouse extends Animal
 */
public class Mouse extends Animal {
    /**
     * What Does MouseSay?
     */
    private String whatDoesMouseSay = "Piiii!";
    /**
     * What Does Mouse Eat?
     */
    private String whatDoesMouseEat = "Cheese!";

    public String getWhatDoesMouseSay() {
        return whatDoesMouseSay;
    }

    public String getWhatDoesMouseEat() {
        return whatDoesMouseEat;
    }
}