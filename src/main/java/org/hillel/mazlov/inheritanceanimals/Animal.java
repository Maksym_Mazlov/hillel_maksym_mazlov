package org.hillel.mazlov.inheritanceanimals;

/**
 * class Animal
 */
//Родительський класс (parent class)
public class Animal {
    /**
     * Quantity of legs
     */
    private static int legs = 4;
    /**
     * What breathes?
     */
    public static final String BREEZE = "Air";
    /**
     * Returns quantity of legs
     *
     * @return
     */
    //Метод выводит кол-во лап
    protected Integer animalLegs() {
        return this.legs;
    }
}