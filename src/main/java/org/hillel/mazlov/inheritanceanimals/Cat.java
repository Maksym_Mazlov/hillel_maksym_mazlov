package org.hillel.mazlov.inheritanceanimals;

/**
 * class Cat extends Animal
 */
public class Cat extends Animal {
    /**
     * What Does Cat Say?
     */
    public String whatDoesCatSay = "Meow!";
    /**
     * What Does Cat Eat?
     */
    public String whatDoesCatEat = "Fish!";
}