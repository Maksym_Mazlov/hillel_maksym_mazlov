package org.hillel.mazlov.inheritanceanimals;

public class Dog extends Animal {

    public String whatDoesDogSay = "Woof!";
    public String whatDoesDogEat = "Bones!";
}