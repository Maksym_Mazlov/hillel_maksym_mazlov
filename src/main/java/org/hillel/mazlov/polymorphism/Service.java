package org.hillel.mazlov.polymorphism;

public interface Service {
    String getName();
    int getPayBonus();
}