package org.hillel.mazlov.polymorphism;

public class OneTimeService implements Service {

    private String name;
    private int payBonus;

    public OneTimeService(String name, int payBonus) {
        this.name = name;
        this.payBonus = payBonus;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getPayBonus() {
        return payBonus;
    }
}