package org.hillel.mazlov.polymorphism;

import javax.xml.crypto.Data;

/**
 * class User
 */
public class User {

    protected User() {

    }

    private String firstName;
    private String lastName;
    private String loginId;
    private String password;
    private String email;
    private Data registered;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLoginId() {
        return loginId;
    }

    //    public void setLoginId(String loginId) {
//        this.loginId = loginId;
//    }
    public void setLoginId(String loginId) {
        if (loginId.matches("^[a-zA-Z][a-zA-Z0-9]*$")) { //^начало строки, *$ - конец строки
            this.loginId = loginId;
        } else {
            System.err.println("Error!");
        }

    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        if (password.length() < 6) {
            System.err.println("Password is too short"); // err - красным в консоль как ошибка.
        } else {
            this.password = password;
        }
    }

//    public void setPassword(String password) {
//        if (password == null || password.length() < 6) {
//            System.err.println("Password is too short"); // err - красным в консоль как ошибка.
//        } else {
//            this.password = password;
//        }
//    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Data getRegistered() {
        return registered;
    }

    public void setRegistered(Data registered) {
        this.registered = registered;
    }
}