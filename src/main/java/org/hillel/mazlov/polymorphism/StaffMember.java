package org.hillel.mazlov.polymorphism;

import java.util.Comparator;

abstract public class StaffMember {

    public static final Comparator<StaffMember> nameComparator = new Comparator<StaffMember>() {
        @Override
        public int compare(StaffMember o1, StaffMember o2) {
            return o1.name.compareTo(o2.name);
        }
    };

    public static final Comparator<StaffMember> addressComparator = new Comparator<StaffMember>() {
        @Override
        public int compare(StaffMember o1, StaffMember o2) {
            return o1.address.compareTo(o2.address);
        }
    };

    public static final Comparator<StaffMember> phoneComparator = new Comparator<StaffMember>() {
        @Override
        public int compare(StaffMember o1, StaffMember o2) {
            return o1.phone.compareTo(o2.phone);
        }
    };

    protected String name;
    protected String address;
    protected String phone;

    public StaffMember(String name, String address, String phone) {
        this.name = name;
        this.address = address;
        this.phone = phone;
    }

    public String toString() {
        String result = "Name: " + name + "\n";
        result += "Address: " + address + "\n";
        result += "Phone: " + phone + "\n";
        return result;
    }

    public abstract double pay();
}