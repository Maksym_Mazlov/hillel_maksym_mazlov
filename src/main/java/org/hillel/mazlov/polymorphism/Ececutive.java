package org.hillel.mazlov.polymorphism;

public class Ececutive extends Employee {
    private double awardBonus;

    public Ececutive(String name, String address, String phone, String socialSecurityNumber, double payRate) {
        super(name, address, phone, socialSecurityNumber, payRate);
        this.awardBonus = 0;
    }

    public void awardBonus(double exexbonus) {
        awardBonus = exexbonus;
    }

    @Override
    public double pay() {
        double payment = super.pay() + awardBonus;
        awardBonus = 0;
        return payment;
    }
}