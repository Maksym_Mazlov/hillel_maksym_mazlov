package org.hillel.mazlov.polymorphism;

public class Hourly extends Employee {
    private int hoursWorked;

    public Hourly(String name, String address, String phone, String socialSecurityNumber, double payRate) {
        super(name, address, phone, socialSecurityNumber, payRate);
        hoursWorked = 0;
    }

    public void addHours(int moreHours) {
        hoursWorked += moreHours;
    }

    @Override
    public double pay() {
        double payment = payRate * hoursWorked;
        int totalBonus = 0;
        if (bonus) {
            for (int i = 0; i < services.length; i++) {
                totalBonus += services[i].getPayBonus() * hoursWorked;
                System.out.println("For service " + services[i].getName() + " obtain a bonus : " + services[i].getPayBonus() * hoursWorked + " for " + hoursWorked + " hours");
            }
            System.out.println();
        }
        hoursWorked = 0;
        return payment + totalBonus;
    }

    @Override
    public String toString() {
        String result = super.toString();
        result += "\nCurrent hours: " + hoursWorked;
        return result;
    }
}