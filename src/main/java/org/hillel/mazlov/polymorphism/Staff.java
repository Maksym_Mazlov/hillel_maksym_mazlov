package org.hillel.mazlov.polymorphism;

import java.util.ArrayList;

public class Staff {
    ArrayList<StaffMember> staffList;

    public Staff() {
        Service referral = new OneTimeService("Hiring a new employee", 500);
        Service presentation = new OneTimeService("Presentation", 250);
        Service businessTrip = new OneTimeService("Business trip", 1000);
        Service overTime = new HourlyService("Over time", 50);

        staffList = new ArrayList<>();

        Ececutive same = new Ececutive("Sam", "123 Main Line", "555-505-555", "000001", 2423.07);
        same.bonus = true;
        same.services = new Service[]{referral, businessTrip, presentation};
        staffList.add(same);

        Employee carla = new Employee("Carla", "123 Off Line", "245-555-555", "000002", 1254.015);
        carla.bonus = true;
        carla.services = new Service[]{referral, presentation};
        staffList.add(carla);

        Employee woody = new Employee("Woody", "123 Off Rocer", "525-555-555", "000003", 1152.23);
        woody.bonus = false;
        staffList.add(woody);

        Hourly diane = new Hourly("Diane", "658 Suda Ave", "105-555-000", "000004", 10.55);
        diane.bonus = true;
        diane.addHours(5);
        diane.services = new Service[]{overTime};
        staffList.add(diane);

        Volunteer norm = new Volunteer("Norm", "952 Main Bldf", "555-555-555");
        staffList.add(norm);

        Volunteer cliff = new Volunteer("Cliff", "123 Main Line", "555-555-555");
        staffList.add(cliff);

        ((Ececutive) staffList.get(0)).awardBonus(500.00);
    }

    public void payday() {
        double amount;
        for (int count = 0; count < staffList.size(); count++) {
            System.out.println(staffList.get(count));
            amount = staffList.get(count).pay();

            if (amount == 0.0) {
                System.out.println("Thanks!");
            } else {
                System.out.println("Paid: " + amount);
            }
            System.out.println("--------------------------------------");
        }
    }
}