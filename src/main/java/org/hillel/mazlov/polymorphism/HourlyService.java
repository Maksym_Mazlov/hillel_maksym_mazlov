package org.hillel.mazlov.polymorphism;

public class HourlyService extends OneTimeService {

    public HourlyService(String name, int bonus) {
        super(name, bonus);
    }
}