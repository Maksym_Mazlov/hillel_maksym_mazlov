package org.hillel.mazlov.polymorphism;

public class Employee extends StaffMember {

    protected String socialSecurityNumber;
    protected double payRate;
    protected boolean bonus;
    protected Service[] services;

    public Employee(String name, String address, String phone, String socialSecurityNumber, double payRate) {
        super(name, address, phone);
        this.socialSecurityNumber = socialSecurityNumber;
        this.payRate = payRate;
    }

    public String toString() {
        String result = super.toString();
        result += "\nSocial Security Number: " + socialSecurityNumber;
        return result;
    }

    /**
     * Return the pay rate for this employee
     *
     * @return
     */
    @Override
    public double pay() {
        int totalBonus = 0;
        if (bonus) {
            for (int i = 0; i < services.length; i++) {
                totalBonus += services[i].getPayBonus();
                System.out.println("For service " + services[i].getName() + " obtain a bonus : " + services[i].getPayBonus());
            }
            System.out.println();
        }
        return payRate + totalBonus;
    }
}