package org.hillel.mazlov.polymorphism;

import java.util.Comparator;

public class EmployeePayRateComparator implements Comparator<Employee> {

    @Override
    public int compare(Employee o1, Employee o2) {
        return (int) (o1.payRate - o2.payRate);
    }
}