package org.hillel.mazlov.multithreading;

import java.util.concurrent.TimeUnit;

public class CuncurrencyThreads {
    public static void main(String[] args) {
//        Runnable task = () -> {
//                Thread.currentThread().setName("Vasua");
//        String threadName = Thread.currentThread().getName();
//        System.out.println(" Hello " + threadName);
//        };
//
//        task.run();
//
//        Thread thread = new Thread(task);
//        thread.start();
//
//        System.out.println("Done!");
        Runnable runnable = () -> {
            try {
                String name = Thread.currentThread().getName();
                System.out.println("Foo " + name);
                TimeUnit.SECONDS.sleep(1);
                System.out.println(" Ber " + name);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        };

        Thread thread = new Thread(runnable);
        thread.start();
    }
}
