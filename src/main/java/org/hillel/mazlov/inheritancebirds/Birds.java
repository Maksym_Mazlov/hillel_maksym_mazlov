package org.hillel.mazlov.inheritancebirds;

import org.hillel.mazlov.inheritanceanimals.Animal;

public class Birds extends Animal {

    String birdsSay = "Piu";

    public String getBirdsSay() {
        return birdsSay;
    }

    public Integer animalLegs() {
        return super.animalLegs() - 2;
    }
}