package org.hillel.mazlov.inner;

public class LocalInner {
    private String message = "Alex.com";

    void display() {
        final int DATA = 20;

        class Local {
            void msg() {
                System.out.println(message + " : " + DATA);
            }
        }

        Local local = new Local();
        local.msg();
    }
}