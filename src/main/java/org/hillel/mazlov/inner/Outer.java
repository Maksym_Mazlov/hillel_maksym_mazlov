package org.hillel.mazlov.inner;

public class Outer {
    private static String textNested = "Nested String!";
    private String text = "I am private!";

    public static class Nested {

        public void printNestedText() {
            System.out.println(textNested);
        }
    }

    public class Inner {
        private String text = "I am Inner private!";

        public void printText() {
            System.out.println(text);
            System.out.println(Outer.this.text);
        }
    }

    public void test() {

    }

    //Локальный класс - класс в методе
    public void loval() {
        class Local {

        }
        Local locale = new Local();
    }

    public void doIt() {
        System.out.println("Outer Class doIt");
    }
}