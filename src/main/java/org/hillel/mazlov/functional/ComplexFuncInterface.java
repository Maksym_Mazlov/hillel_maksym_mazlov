package org.hillel.mazlov.functional;

public interface ComplexFuncInterface extends SimpleFuncInterface {
    static void doSomeWork() {
        System.out.println("Doing some work in interface impl...");
    }

    default void doSomeOtherWork() {
        System.out.println("Doing some OTHER work in interface impl...");
    }
}
