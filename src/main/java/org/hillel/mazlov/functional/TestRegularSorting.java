package org.hillel.mazlov.functional;

import java.util.*;

public class TestRegularSorting {
    public static void main(String[] args) {
        List<Developer> listDev = getDevelopers();

        System.out.println("<-------Before sort------->");
        for (Developer developer : listDev) {
            System.out.println(developer);
        }

        //sort by age regular
        Collections.sort(listDev, new Comparator<Developer>() {
            @Override
            public int compare(Developer o1, Developer o2) {
                return o1.getAge() - o2.getAge();
            }
        });

        System.out.println("<-------After sort------->");
        for (Developer developer : listDev) {
            System.out.println(developer);
        }

    }

    private static List<Developer> getDevelopers() {
        List<Developer> result = new ArrayList<>();
        result.add(new Developer("Max", 10000, 33));
        result.add(new Developer("Vanya", 20000, 15));
        result.add(new Developer("Irisha", 177777, 10));
        result.add(new Developer("Jason", 30000, 20));
        return result;
    }
}
