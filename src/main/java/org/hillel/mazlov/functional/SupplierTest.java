package org.hillel.mazlov.functional;

import java.util.Scanner;
import java.util.function.Supplier;

public class SupplierTest {
    public static void main(String[] args) {
        Supplier<User> userSupplier = () -> {
            Scanner in = new Scanner(System.in);
            System.out.println("Get Name: ");
            String name = in.nextLine();
            return new User(name);
        };

        User user1 = userSupplier.get();
        User user2 = userSupplier.get();

        System.out.println("Name user1: " + user1.getName());
        System.out.println("Name user2: " + user2.getName());
    }
}
