package org.hillel.mazlov.functional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TesrLambdaSorting {

    public static void main(String[] args) {

        List<Developer> listDev = getDevelopers();

        System.out.println("<-------Before sort------->");
        for (Developer developer : listDev) {
            System.out.println(developer);
        }

        //sort by java 8

        //lambda here!
        System.out.println("<-------After sort java 8------->");
        listDev.sort(((o1, o2) -> o1.getAge() - o2.getAge()));

        //java 2 only, lambda also, to print the list
        listDev.forEach((developer) -> System.out.println(developer));
    }

    private static List<Developer> getDevelopers() {
        List<Developer> result = new ArrayList<>();
        result.add(new Developer("Max", 10000, 33));
        result.add(new Developer("Vanya", 20000, 15));
        result.add(new Developer("Irisha", 177777, 10));
        result.add(new Developer("Jason", 30000, 20));
        return result;
    }
}
