package org.hillel.mazlov.functional;

@FunctionalInterface
public interface SimpleFuncInterface {
    void doWork();
    String toString();
    boolean equals(Object o);
}
