package org.hillel.mazlov.java8.homework;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        Path path = Paths.get("./src/main/resources/homework_test.txt");

        try {
            Stream<String> fileLines = Files.lines(path);
            long wordCount = fileLines.flatMap(line -> Arrays.stream(line.split(" "))).count();
            System.out.println(wordCount);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}