package org.hillel.mazlov.string;

import java.util.Arrays;
import java.util.Random;

/**
 * Class String Data
 */
public class StringData {

    protected StringData() {
    }

    /**
     * Start program
     *
     * @param args
     */
    public static void main(String[] args) {
        String str = "Hello World";

        char[] chr = str.toCharArray();

        for (int i = 0; i < chr.length; i++) {
            System.out.print("[" + chr[i] + "]");
        }
        System.out.println();
        System.out.println("---------------------------------------");

        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == ' ') {
                System.out.print(" ");
            } else {
                System.out.print("[" + str.charAt(i) + "]");
            }
        }
        System.out.println();
        System.out.println("---------------------------------------");

        char[] c = {'H', 'E', 'L', 'L', 'O'};
        String constructorString = new String(c); //Строка через передачу символов в конструктор
        System.out.println(constructorString);

        char[] ch = constructorString.toCharArray(); //Масив символо из строки
        for (int i = 0; i < ch.length; i++) {
            System.out.print(ch[i]); //Перебираем масив символов по индексу
        }
        System.out.println();
        System.out.println("---------------------------------------");

        // Переворачиваеи масив символов
        for (int i = 0; i < ch.length / 2; i++) {
            char tmp = ch[i];
            ch[i] = ch[ch.length - i - 1];
            ch[ch.length - i - 1] = tmp;
        }

        for (int i = 0; i < ch.length; i++) { // Вывод результата
            System.out.print(ch[i]);
        }
        System.out.println();
        System.out.println("---------------------------------------");

        // StringBuffer - небезопасно но быстро, лучше для многопотока
        StringBuffer stringBuffer = new StringBuffer("Hello ok");
        System.out.println(stringBuffer);

        String revertString = new StringBuffer("Maxim").reverse().toString();
        System.out.println(revertString);

        System.out.println();
        System.out.println("---------------------------------------");
        String first = "Milan";
        String second = "Berlin";
        System.out.println(first + second);

        System.out.println("---------------------------------------");

        String third;
        third = first.concat(second);
        System.out.println(third + " !!! ");
        System.out.println(first.concat(second));
        System.out.println(first);

        System.out.println("---------------------------------------");

        System.out.println(second.endsWith("in")); // Да, если заканчивається строка на in
        System.out.println(first.contains("il")); // Да, если есть в строке il

        System.out.println("---------------------------------------");

        System.out.println(first.toUpperCase()); // Переобразование строки БОЛЬШИМИ

        System.out.println(first.replace('M', 'B') + second.replace('B', 'M')); // Замена символов

        System.out.println("---------------------------------------");

        Employee employee = new Employee();
        employee.setName("Ivan");
        employee.setAge(25);
        employee.setSalary(10000);
        System.out.println(employee.toString());

        System.out.println("---------------------------------------");

        String splitString = "We love New Year";
        String[] splitStringArray = splitString.split(" "); //Из строки в масив по разделителю
        System.out.println(Arrays.toString(splitStringArray));

        System.out.println("---------------------------------------");

        //Перебрали масив и поменяли символ на последний каждого слова
        for (int i = 0; i < splitStringArray.length; i++) {
            splitStringArray[i] = splitStringArray[i].replace(splitStringArray[i].charAt(0),
                    splitStringArray[i].charAt(splitStringArray[i].length() - 1));
        }
        System.out.println(Arrays.toString(splitStringArray));

        System.out.println("---------------------------------------");
        StringBuffer stringBuf = new StringBuffer("New Word");
        System.out.println(stringBuf);
        System.out.println(stringBuf.capacity());

        System.out.println("---------------------------------------");

        String strFirstPart = "Товар стоит ";
        String strSecondPart = " баксов";
        int sum = 10;

        StringBuffer sb = new StringBuffer();
        sb.append(strFirstPart).append(sum).append(strSecondPart); //append - Добавляет и строки и цифри.
        System.out.println(sb);

        System.out.println("---------------------------------------");
        String[] stringsArray = {"My name is Petia", "I am president Ukraine", " I have factory"};
        StringBuffer buffer = new StringBuffer();

        for (int i = 0; i < stringsArray.length; i++) {
            buffer.append("[" + stringsArray[i] + "]");
        }
        System.out.println(buffer);

        System.out.println("---------------------------------------");
        //Меняем строки местами в массиве строк
        String[] changeWorlds = {"Vacya", " Petia", " LoL"};
        System.out.println(Arrays.toString(changeWorlds));

        String tmp = changeWorlds[0];
        changeWorlds[0] = changeWorlds[changeWorlds.length - 1];
        changeWorlds[changeWorlds.length - 1] = tmp;
        System.out.println(Arrays.toString(changeWorlds));

        System.out.println("---------------------------------------");
        System.out.println(StringData.createdRandomString());
    }

    //Генерируем пароль
    /**
     * String to create a password
     */
    private static final String M_CHAR = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    /**
     * Length password
     */
    private static final int STR_LENGTH = 13; //длина генерируемой строки

    /**
     * @return create password and return
     */
    public static String createdRandomString() {
        Random random = new Random();
        StringBuilder builder = new StringBuilder();

        System.out.println(M_CHAR.length());

        for (int i = 0; i < STR_LENGTH; i++) {
            int number = random.nextInt(M_CHAR.length());
            char ch = M_CHAR.charAt(number);
            builder.append(ch);
        }
        return builder.toString();
    }
}