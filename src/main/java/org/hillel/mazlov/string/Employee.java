package org.hillel.mazlov.string;

/**
 * Class Employee
 */
public class Employee {
    /**
     * Name
     */
    private String name;
    /**
     * Age
     */
    private Integer age;
    /**
     * Salary
     */
    private Integer salary;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getSalary() {
        return salary;
    }

    public void setSalary(Integer salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Employee{" + "name = " + name + ", age = " + age + ", salary = " + salary + "}";
    }
}