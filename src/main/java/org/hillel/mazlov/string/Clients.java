package org.hillel.mazlov.string;

/**
 * class Clients
 */
public class Clients {
    /**
     * Clients password
     */
    private String password;

    public String getParol() {
        return password;
    }

    public void setParol(String parol) {
        this.password = parol;
    }
}