package org.hillel.mazlov.string;

import java.util.StringTokenizer;

/**
 * Class StringTokenizer
 */
public class StringTokenizerExample {

    protected StringTokenizerExample() {
    }

    /**
     * split string by space
     *
     * @param str string
     */
    public void splitBySpace(String str) {
        StringTokenizer st = new StringTokenizer(str);
        System.out.println("---- Split by space ----");

        while (st.hasMoreElements()) {
            System.out.println(st.nextElement());
        }
    }

    /**
     * split string by comma
     *
     * @param str string
     */
    public void splitByComma(String str) {
        System.out.println("---Split by Comma ',' -----");
        StringTokenizer st = new StringTokenizer(str, ",");

        while (st.hasMoreElements()) {
            System.out.println(st.nextElement());
        }
    }
}
