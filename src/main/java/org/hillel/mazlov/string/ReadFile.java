package org.hillel.mazlov.string;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

/**
 * class ReadFile
 */
public class ReadFile {

    protected ReadFile() {
    }

    /**
     * Read files_data
     *
     * @param path path to the files_data
     */
    public static void readCsv(String path) {
        BufferedReader br = null;

        try {
            String line;
            br = new BufferedReader(new FileReader(path));

            while ((line = br.readLine()) != null) {
                System.out.println(line);

                StringTokenizer stringTokenizer = new StringTokenizer(line, "|");

                while (stringTokenizer.hasMoreElements()) {
                    Integer id = Integer.parseInt(stringTokenizer.nextElement().toString());
                    Double price = Double.parseDouble(stringTokenizer.nextElement().toString());
                    String userName = stringTokenizer.nextElement().toString();

                    StringBuilder sb = new StringBuilder();

                    sb.append("\nId : " + id);
                    sb.append("\nprice : " + price);
                    sb.append("\nuserName : " + userName);
                    sb.append("\n*****************\n");

                    System.out.println(sb.toString());
                }
            }

            System.out.println("Done");

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
