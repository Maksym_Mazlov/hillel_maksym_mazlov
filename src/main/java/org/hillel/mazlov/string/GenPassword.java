package org.hillel.mazlov.string;

import java.util.Arrays;
import java.util.Random;

/**
 * Class GenPassword
 */
public class GenPassword {
    protected GenPassword() {
    }

    /**
     * generation password and return
     *
     * @return
     */
    public static String genPassword() {

        String strFirst = "Вечірнє сонце, дякую за день! Вечірнє сонце, дякую за втому.";
        String strSecond = "За тих лісів просвітлений Едем і за волошку в житі золотому.";

        String strPaired = "";
        String strUnpaired = "";

        for (int i = 0; i < strFirst.length(); i++) {
            if (i % 2 == 0) {
                strPaired += strFirst.charAt(i);
            }
        }
        for (int i = 0; i < strSecond.length(); i++) {
            if (i % 2 == 0) {
                strPaired += strSecond.charAt(i);
            }
        }

        String strThird = "За твій світанок, і за твій зеніт, і за мої обпечені зеніти.";
        String strFourth = "За те, що завтра хоче зеленіть, за те, що вчора встигло оддзвеніти.";

        for (int i = 0; i < strThird.length(); i++) {
            if (i % 2 != 0) {
                strUnpaired += strThird.charAt(i);
            }
        }
        for (int i = 0; i < strFourth.length(); i++) {
            if (i % 2 != 0) {
                strUnpaired += strFourth.charAt(i);
            }
        }

        StringBuilder builder = new StringBuilder();
        String strCombined = (builder.append(strPaired).append(strUnpaired)).toString();

        int strLength = 40;
        Random random = new Random();
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < strLength; i++) {
            int number = random.nextInt(strCombined.length());
            char ch = strCombined.charAt(number);

            stringBuilder.append(ch);

            if (i == 9) {
                stringBuilder.append('@');
            }
            if (i == 19) {
                stringBuilder.append('@');
            }
            if (i == 29) {
                stringBuilder.append('@');
            }
            if (i == 39) {
                stringBuilder.append('@');
            }
        }

        String str = stringBuilder.toString();

        String[] strArray = str.split("@");
        Arrays.toString(strArray);

        int num = random.nextInt(strArray.length);
        String password = strArray[num];

        return password;
    }
}