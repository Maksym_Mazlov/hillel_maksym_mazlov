package org.hillel.mazlov.maps;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class TreeMapExample {
    public static void main(String[] args) {
        Map<String, String> treeMap = new TreeMap<>();
        treeMap.put("Bruce", "Willis");
        treeMap.put("Arnold", "Schwarz");
        treeMap.put("Jackie", "Chan");
        treeMap.put("Sylvester", "Stallone");
        treeMap.put("Chuck", "Norris");

        for (Map.Entry tree : treeMap.entrySet()) {
            System.out.println(tree.getKey() + "  " + tree.getValue());
        }
        System.out.println("------------------------------");
        TreeMap<String, Double> tradeMap = new TreeMap<>();
        tradeMap.put("Zara", 1000.00);
        tradeMap.put("Boss", 250.05);
        tradeMap.put("Cropp", 9999.99);
        tradeMap.put("Koko", -55.55);

        tradeMap.forEach((key, value) -> {
            System.out.println(key + " = " + value);
        });
        System.out.println("------------------------------");
        Set set = tradeMap.entrySet();

        Iterator iterator = set.iterator();

        while (iterator.hasNext()) {
            Map.Entry entryMap = (Map.Entry) iterator.next();
            System.out.print(entryMap.getKey() + " : ");
            System.out.println(entryMap.getValue());
        }
        System.out.println("------------------------------");

        double balance = tradeMap.get("Zara");
        tradeMap.put("Zara", balance + 1000);
        System.out.println("Zara balance : " + tradeMap.get("Zara"));

        Map<User, String> userMap = new TreeMap<>(new UserSalaryComparator());
        userMap.put(new User("Max", "Mazlov", 99999), "My name 1");
        userMap.put(new User("Vasya", "Kurack", 5), "My name 2");
        userMap.put(new User("Petya", "Blabla", 4555), "My name 3");
        userMap.put(new User("Vasilisa", "Foina", 1), "My name 4");
        System.out.println(userMap.toString());

        Set keySet = userMap.entrySet();
        for (Object user : keySet) {
            System.out.println(user);
        }
    }
}