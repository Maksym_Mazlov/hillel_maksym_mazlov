package org.hillel.mazlov.maps;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class LinkedHashMapExample {
    public static void main(String[] args) {
        Map<String, Double> linkedHashMap = new HashMap<>();
        linkedHashMap.put("Apple", new Double(91.88));
        linkedHashMap.put("Sony", new Double(16.45));
        linkedHashMap.put("Dell", new Double(12.55));
        linkedHashMap.put("HP", new Double(138.44));
        linkedHashMap.put("IBM", new Double(15.08));

        System.out.println("Contents of LinkedHashMap : " + linkedHashMap);
        System.out.println("\nValues of map after iterating over it : ");
        for (String key : linkedHashMap.keySet()) {
            System.out.println(key + "\t" + linkedHashMap.get(key));
        }

        System.out.println(linkedHashMap.size());

        System.out.println(linkedHashMap.isEmpty());

        System.out.println("------------------------------");

        Iterator<Map.Entry<String, Double>> it = linkedHashMap.entrySet().iterator();
        while (it.hasNext())
            System.out.println(it.next());

        System.out.println("\nLinkedHashMap contains Sony as Key? : " + linkedHashMap.containsKey("Sony"));
        System.out.println(linkedHashMap.containsValue(33.66));

        System.out.println(linkedHashMap.entrySet());
        System.out.println(linkedHashMap.remove("HP"));
        System.out.println(linkedHashMap.entrySet());

        System.out.println("------------------------------");
        linkedHashMap.clear();
        System.out.println(linkedHashMap);
    }
}