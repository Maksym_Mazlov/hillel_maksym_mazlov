package org.hillel.mazlov.maps;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class HashMapExample {
    public static void main(String[] args) {
        //K - это ключ (Key) V - значения Value
        Map<Integer, String> map = new HashMap();
        map.put(1, "I live in Kiev");
        map.put(2, "I live in Lviv");
        map.put(3, "I live in Madrid");
        map.put(4, "I live in London");
        map.put(5, "I live in Ankara");

        //values in array type
        System.out.println(map.values());
        System.out.println(map.entrySet());
        System.out.println(map.keySet());
        System.out.println(map);
        System.out.println(map.isEmpty());

        System.out.println("------------------------------");
        for (int i = 0; i < map.size(); i++) { //у мапи не має ключа "0" а є тільки 1 2 3 4 5  тому null
            System.out.println(map.get(i));
        }

        System.out.println(map.entrySet());
        map.replace(3, "HELLO");
        System.out.println(map.entrySet());
        System.out.println("------------------------------");

        map.remove(3);
        System.out.println(map.entrySet());
        System.out.println("------------------------------");

        for (HashMap.Entry objectMap : map.entrySet()) { //  Должен получть каждый Entry с моего списка entrySet
            System.out.println(objectMap.getValue() + " = " + objectMap.getKey());
            objectMap.setValue("Good Bye");
            System.out.println(objectMap.getValue() + " = " + objectMap.getKey());
        }
        System.out.println("------------------------------");

        Map<Integer, String> hashMap = new HashMap<>();
        hashMap.put(1, "January");
        hashMap.put(2, "February");
        hashMap.put(3, "March");
        hashMap.put(4, "April");
        hashMap.put(5, "May");
        hashMap.put(6, "");
        hashMap.put(7, "");
        hashMap.put(8, "");
        hashMap.put(9, "");
        hashMap.put(10, "");
        hashMap.put(11, "");
        hashMap.put(12, "");

        Integer y = null;
        for (HashMap.Entry<Integer, String> mapMonth : hashMap.entrySet()) {
            y = mapMonth.getKey();
            if (mapMonth.getValue().equals("")) {
                switch (y) {
                    case 6:
                        mapMonth.setValue("June");
                        break;
                    case 7:
                        mapMonth.setValue("July");
                        break;
                    case 8:
                        mapMonth.setValue("August");
                        break;
                    case 9:
                        mapMonth.setValue("September");
                        break;
                    case 10:
                        mapMonth.setValue("October");
                        break;
                    case 11:
                        mapMonth.setValue("November");
                        break;
                    case 12:
                        mapMonth.setValue("December");
                        break;
                }
            }
        }
        System.out.println(hashMap.entrySet());
        System.out.println("------------------------------");

        Random random = new Random();
        Map<Integer, Integer> newMap = new HashMap();

        for (int i = 0; i < 100; i++) {
            int num = random.nextInt(10);
            Integer freq = newMap.get(num);
            newMap.put(num, freq == null ? 1 : freq + 1);
        }
        System.out.println(newMap);
    }
}