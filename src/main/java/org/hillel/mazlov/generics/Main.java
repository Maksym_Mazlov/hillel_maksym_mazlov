package org.hillel.mazlov.generics;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Test<String, Integer> test = new Test<>("Maks :", 27);
        List<Test> list = new ArrayList<>();
        list.add(test);
        for (Test s : list) {
            System.out.println(s.getT() + " " + s.getX());
        }

        list.forEach(x->System.out.println(x.getT() + " " +x.getX()));
    }
}
