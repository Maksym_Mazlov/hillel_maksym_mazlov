package org.hillel.mazlov.generics;

public abstract class Animal<T> {
    abstract T name(T t);

    abstract T getData();

    public void text(){
        System.out.println("This is Animal!");
    }
}
