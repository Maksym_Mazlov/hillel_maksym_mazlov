package org.hillel.mazlov.generics;

import java.util.ArrayList;
import java.util.List;

public class WildCardExample {
    public static void main(String[] args) {
        List list = new ArrayList();
        list.add(5);
        list.add(56);
        list.add(5.8);
        Double x = sum(list);
        System.out.println(x);

    }

    public static Double sum (List<? extends Number> sumList){
        Double sum = 0.0;
        for (Number number: sumList){
            sum = sum + number.doubleValue();
        }
        return sum;
    }
}
