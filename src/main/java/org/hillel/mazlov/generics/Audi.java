package org.hillel.mazlov.generics;

public class Audi extends Car{

    private String model;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
