package org.hillel.mazlov.generics;

public class Test <T,X> {
private T t;
private X x;

    public Test(T t, X x) {
        this.t = t;
        this.x = x;
    }

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    public X getX() {
        return x;
    }

    public void setX(X x) {
        this.x = x;
    }
}


