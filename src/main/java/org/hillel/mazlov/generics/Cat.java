package org.hillel.mazlov.generics;

public class Cat extends Animal {
    @Override
    public Object name(Object o) {
        return this;
    }

    @Override
    public Object getData() {
        return null;
    }
}
