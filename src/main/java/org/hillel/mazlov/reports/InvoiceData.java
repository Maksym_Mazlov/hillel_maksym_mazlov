package org.hillel.mazlov.reports;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class InvoiceData {

    private Invoice invoice;

    public InvoiceData() {
        invoice = createdInvoice();
    }

    private Item createItem(String des, Integer integer, BigDecimal bigDecimal) {
        Item item = new Item();
        item.setDescription(des);
        item.setQuantity(integer);
        item.setUnitprice(bigDecimal);
        return item;
    }

    private Customer createCustomer(String name, String address, String city, String email) {
        Customer customer = new Customer();
        customer.setName(name);
        customer.setAddress(address);
        customer.setCity(city);
        customer.setEmail(email);
        return customer;
    }

    private Invoice createdInvoice() {
        Invoice invoice = new Invoice();
        invoice.setId(5);
        invoice.setShipping(new BigDecimal(10));
        invoice.setTax(0.2);

        invoice.setBillTo(createCustomer("Mary Patterson", "151 Pompton St.", "Washington", "sdfds@gmail"));
        invoice.setShipTo(createCustomer("Vasya", "1007 Baden St.", "New York", "sdfds@gmail"));

        List<Item> items = new ArrayList<>();
        items.add(createItem("NoteBook", 1, new BigDecimal(1000)));
        items.add(createItem("Dvd", 5, new BigDecimal(50)));
        items.add(createItem("Book", 4, new BigDecimal(10)));
        items.add(createItem("Phone", 18, new BigDecimal(200)));
        invoice.setItems(items);
        return invoice;
    }

    public Invoice getInvoice() {
        return invoice;
    }

    public JRDataSource createDataSource() {
        return new JRBeanCollectionDataSource(invoice.getItems());
    }
}