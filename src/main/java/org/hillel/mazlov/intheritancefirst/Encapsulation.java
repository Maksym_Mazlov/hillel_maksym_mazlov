package org.hillel.mazlov.intheritancefirst;

public class Encapsulation {
    private int x = 10;
    protected int x1;
    public int x2;
    int x3;

    public int getX() {
        return x;
    }
}