package org.hillel.mazlov;

import static java.lang.System.out;

/**
 * Created Maksym Mazlov
 */
public class HelloWorld {

    protected HelloWorld() {

    }

    /**
     * Our versoin for System.out.print
     * @param t
     * @param <T>
     */
    public static <T> void print(T t) {
        out.print(t);
    }

    /**
     * Our versoin for System.out.println
     * @param object
     */
    public static void println(Object object) {
        out.println(object);
    }


    /**
     * Our versoin for System.out.println
     */
    public static void println() {
        out.println();
    }
}

