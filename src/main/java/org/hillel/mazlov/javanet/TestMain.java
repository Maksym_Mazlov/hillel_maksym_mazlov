package org.hillel.mazlov.javanet;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicInteger;

public class TestMain {
    public static AtomicInteger atomicInteger = new AtomicInteger(0);

    public static void main(String[] args) throws Exception {
        String mainLink = "https://rozetka.com.ua";
        List<String> stringList = new ArrayList<>();
        Integer threadQuanity = 10;

        Document document = Jsoup.connect(mainLink).get();
        Elements linkTags = document.select("a[href]");

        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(threadQuanity);

        String link;
        for (Element elements : linkTags) {
            link = elements.attr("href");
            if (link.startsWith("https://rozetka.com.ua")) {
                stringList.add(link);
            }
        }
        for (int i = 0; i < threadQuanity; i++) {
            ThreadsController threadsController = new ThreadsController(stringList.get(i));
            executor.execute(threadsController);
        }
        executor.shutdown();
    }
}
