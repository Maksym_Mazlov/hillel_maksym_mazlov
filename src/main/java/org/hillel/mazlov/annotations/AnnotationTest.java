package org.hillel.mazlov.annotations;

@TestInfo(
        priority = TestInfo.Priority.HIGHT,
        createdBy = "Dima",
        tags = {"sales", "test"}
)
public class AnnotationTest {

    @Test
    void testA() {
        if (true)
            throw new RuntimeException("This test always failed");
    }

    @Test(enabled = false)
    void testB() {
        if (false)
            throw new RuntimeException("Tjis test always passed");

    }

    @Test(enabled = true)
    void testC() {
        if (10 > 1) {
            //do nothing, this test always passed.
        }
    }
}