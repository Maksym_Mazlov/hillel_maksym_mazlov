package org.hillel.mazlov.annotations;

import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface MyNewComponent {

    String value();

    String name();

    String age();

    String[] newName();

    String element() default "elem";
}
