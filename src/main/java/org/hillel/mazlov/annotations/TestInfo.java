package org.hillel.mazlov.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface TestInfo {
    enum Priority {
        LOW, MEDIUM, HIGHT
    }

    Priority priority() default Priority.MEDIUM;

    String[] tags() default "";

    String createdBy() default " Max";

    String lastModified() default "08/08/2088";
}
