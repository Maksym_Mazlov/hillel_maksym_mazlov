package org.hillel.mazlov.annotations;

import java.io.Serializable;

public class People implements Serializable, Cloneable {
    private String name;
    private int age;
    private int sum;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getSum() {
        return sum;
    }

    public void setSum(int sum) {
        this.sum = sum;
    }

    @Deprecated
    protected static void method(String[] params) {

    }
}
