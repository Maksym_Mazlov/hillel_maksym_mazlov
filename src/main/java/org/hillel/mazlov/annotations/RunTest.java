package org.hillel.mazlov.annotations;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

public class RunTest {
    public static void main(String[] args) throws Exception {
        System.out.println("Testing...");
        int passed = 0, failed = 0, count = 0, ignore = 0;

        Class<AnnotationTest> obj = AnnotationTest.class;

        if (obj.isAnnotationPresent(TestInfo.class)) {
            Annotation annotation = obj.getAnnotation(TestInfo.class);
            TestInfo testInfo = (TestInfo) annotation;

            System.out.printf("%nPriority :%s", testInfo.priority());
            System.out.printf("%nCreated :%s", testInfo.createdBy());
            System.out.printf("%nTags :");

            int tagLenght = testInfo.tags().length;

            for (String tag :
                    testInfo.tags()) {
                if (tagLenght > 1) {
                    System.out.print(tag + " ");
                } else {
                    System.out.print(tag);
                }
                tagLenght--;
            }
            System.out.printf("%nLastModified :%s%n%n", testInfo.lastModified());
        }
        for (Method method : obj.getDeclaredMethods()) {
            if (method.isAnnotationPresent(Test.class)) {
                Annotation annotation = method.getAnnotation(Test.class);
                Test test = (Test) annotation;
                //if enabled - true(default)
                if (test.enabled()) {
                    try {
                        method.invoke(obj.newInstance());
                        System.out.printf("%s - Test '%s' - passed %n", ++count, method.getName());
                        passed++;
                    } catch (Throwable ex) {
                        System.out.printf("%s - Test '%s' - failed %s %n", ++count, method.getName(), ex.getCause());
                        failed++;
                    }

                } else {
                    System.out.printf("%s - Test '%s' - ignored %n", ++count, method.getName());
                    ignore++;
                }
            }
        }
        System.out.printf("%nResult : Total : %d,Passed: %d, Failed %d, Ignored %d%n", count, passed, failed, ignore);
    }
}