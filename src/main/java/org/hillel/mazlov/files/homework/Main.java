package org.hillel.mazlov.files.homework;

import java.io.*;

public class Main {

    public static void main(String[] args) throws IOException {
        final String PATH = "/home/maksym/Desktop/Dev/Hillel/src/main/resources/files/1.txt";
        File file = new File(PATH);

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(PATH))) {
            System.out.println("Created file: " + file.getName() + "\n");
            bw.write("Maxim\t" + 25 + "\t" + 3000 + "\n");
            bw.write("Jon\t" + 50 + "\t" + 5000 + "\n");
            bw.write("Vasya\t" + 18 + "\t" + 6000 + "\n");
            bw.write("Juna\t" + 30 + "\t" + 1000 + "\n");
            bw.write("Stasik\t" + 25 + "\t" + 700 + "\t" + "\n");
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        try (BufferedReader br = new BufferedReader(new FileReader(PATH))) {
            String s;
            while ((s = br.readLine()) != null) {
                Employee employee = new Employee();
                String[] line = s.split("\t");
                employee.setName(line[0]);
                employee.setAge(Integer.parseInt(line[1]));
                employee.setSalary(Integer.parseInt(line[2]));
                System.out.println(employee.getName() + " " + employee.getAge() + " " + employee.getSalary());
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
