package org.hillel.mazlov.files;

import java.io.*;
import java.util.regex.Pattern;

public class BufferReaderAndWriterExample {
    public static void main(String[] args) throws IOException {
        final String FILES_PATH = "/home/maksym/Desktop/Dev/Hillel/src/main/resources/test";
        final String FILE_CSV_NAME = "Files.csv";

        File[] files = new File(FILES_PATH).listFiles();
        File fileCsv = new File(FILES_PATH + "/" + FILE_CSV_NAME);

        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(fileCsv, true))) {
            for (File file : files) {
                if (file.getName().endsWith(".srt")) {
                    System.out.println(file.getName());
                    if (file.length() < 10) {
                        file.delete();
                        System.out.println(file.getName() + " del");
                    }
                    bufferedWriter.write("TEST/" + file.getName().replace(".srt", "") +
                            "," + file.getName().replace(".srt", ""));
                    bufferedWriter.newLine();
                }
            }
            bufferedWriter.flush();
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        try {
            srtHoursDur(FILES_PATH);
        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }

    public static void srtHoursDur(String filePath) throws IOException {
        Pattern pattern = Pattern.compile("\\d\\d:\\d\\d:\\d\\d,\\d\\d\\d");
        Pattern pattern2 = Pattern.compile(":\\d\\d,\\d\\d\\d");
        String line;
        String formatTimeLine;
        String[] hourMin;
        Float hour;
        Float mins;
        Float minsInHours;
        Float srtHours;
        Float hoursSum = 0.0f;
        File[] files = new File(filePath).listFiles();
        for (File item : files) {
            if (item.getName().endsWith(".srt")) {
                try (RandomAccessFile reader = new RandomAccessFile(item, "r")) {
                    for (int k = 0; k <= reader.length(); k++) {
                        reader.seek(reader.length() - k);
                        line = reader.readLine();
                        System.out.println(line);

                        if (line != null && pattern.matcher(line).matches()) {
                            formatTimeLine = pattern2.matcher(line).replaceAll("");
                            hourMin = formatTimeLine.split(":");
                            hour = Float.parseFloat(hourMin[0]);
                            mins = Float.parseFloat(hourMin[1]);
                            minsInHours = mins / 60;
                            srtHours = hour + minsInHours;
                            System.out.println(item.getName() + ": ");
                            System.out.print("     ");
                            System.out.println(srtHours + " hours");
                            hoursSum += srtHours;
                            break;
                        }
                    }
                }
            }
        }
        System.out.println("Total: " + hoursSum);
    }
}
