package org.hillel.mazlov.inheritance;

import org.hillel.mazlov.intheritancefirst.Encapsulation;

public class EncapsulationTest2 extends Encapsulation {
    public static void main(String[] args) {
        Encapsulation encapsulation = new Encapsulation();
        encapsulation.x2 = 25;

        EncapsulationTest2 encapsulationTest2 = new EncapsulationTest2();
        //нужно наследоваться от Encapsulation что бы видеть protected поле
        encapsulationTest2.x1 = 15;
        encapsulationTest2.x2 = 23;
        System.out.println(encapsulation.x2);
    }
}