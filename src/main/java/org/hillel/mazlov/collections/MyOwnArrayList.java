package org.hillel.mazlov.collections;

import java.io.Serializable;
import java.util.RandomAccess;

public class MyOwnArrayList implements RandomAccess, Cloneable, Serializable {
    private transient Object[] elementData;//запрещает серилизовать //making one array to hold data
    private int size;
    protected transient int modCount = 0; //Counter to find how many times Array is re-sized
    private static final long serialVersionUID = 1234L;

    public MyOwnArrayList() {
        this(10);
    }

    public MyOwnArrayList(int initialCapacity) {
        super();
        if (initialCapacity < 0) {
            throw new IllegalArgumentException("IllegalArgumentException: " + initialCapacity);
        } else {
            this.elementData = new Object[initialCapacity];
        }
    }

    public Object get(int index) {
        Object o = elementData[index];
        return o;
    }

    public int size() {
        return size;
    }

    public boolean add(Object obj) {
        validateCapacity(size + 1);
        elementData[size++] = obj;
        return true;
    }

    public void validateCapacity(int minCapacity) {
        modCount++;
        int oldCapacity = elementData.length;
        if (minCapacity > oldCapacity) {
            Object oldData[] = elementData;
            int newCapacity = (oldCapacity * 3) / 2; // Size increases by 1.5 times
            if (newCapacity < minCapacity) {
                newCapacity = minCapacity;
            } else {
                elementData = new Object[newCapacity];
                System.arraycopy(oldData, 0, elementData, 0, size); // src,srcPos,dest,destPos,length
            }
        }
    }

    public Object remove(int index) {
        if (index >= size)
            throw new IndexOutOfBoundsException("Index: " + index + ",Size: " + size);
        modCount++;
        Object oldValue = elementData[index];
        int numMoved = size - index - 1;
        if (numMoved > 0) {
            System.arraycopy(elementData, index + 1, elementData, index, numMoved);
        }
        elementData[--size] = null; //Let go to its work
        return oldValue;
    }

    public FruitIterator iterator() {
        System.out.println("My overridded iterator method called in Fruit class");
        return new FruitIterator(this);
    }
}