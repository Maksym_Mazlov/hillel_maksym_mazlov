package org.hillel.mazlov.collections.homework;

import java.io.Serializable;
import java.util.RandomAccess;

public class MyOwnArrayListGen<E> implements RandomAccess, Cloneable, Serializable {
    private transient E[] elementData;//making one array to hold data
    private int size;
    protected transient int modCount = 0; //Counter to find how many times Array is re-sized
    private static final long serialVersionUID = 1234L;

    public MyOwnArrayListGen() {
        this(10);
    }

    public MyOwnArrayListGen(int initialCapacity) {
        super();
        if (initialCapacity < 0) {
            throw new IllegalArgumentException("IllegalArgumentException: " + initialCapacity);
        } else {
            this.elementData = (E[]) new Object[initialCapacity];
        }
    }

    public E get(int index) {
        Object o = elementData[index];
        return (E) o;
    }

    public int size() {
        return size;
    }

    public boolean add(E obj) {
        validateCapacity(size + 1);
        elementData[size++] = obj;
        return true;
    }

    public void validateCapacity(int minCapacity) {
        modCount++;
        int oldCapacity = elementData.length;
        if (minCapacity > oldCapacity) {
            Object oldData[] = elementData;
            int newCapacity = (oldCapacity * 3) / 2; // Size increases by 1.5 times
            if (newCapacity < minCapacity) {
                newCapacity = minCapacity;
            } else {
                elementData = (E[]) new Object[newCapacity];
                System.arraycopy(oldData, 0, elementData, 0, size); // src,srcPos,dest,destPos,length
            }
        }
    }

    public E remove(int index) {
        if (index >= size)
            throw new IndexOutOfBoundsException("Index: " + index + ",Size: " + size);
        modCount++;
        Object oldValue = elementData[index];
        int numMoved = size - index - 1;
        if (numMoved > 0) {
            System.arraycopy(elementData, index + 1, elementData, index, numMoved);
        }
        elementData[--size] = null;
        return (E) oldValue;
    }
}
