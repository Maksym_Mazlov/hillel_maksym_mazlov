package org.hillel.mazlov.collections;

import java.util.Comparator;

public class MyNameComp implements Comparator<Empl> {
    @Override
    public int compare(Empl o1, Empl o2) {
        return o1.getName().compareTo(o2.getName());
    }
}