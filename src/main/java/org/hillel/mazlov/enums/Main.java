package org.hillel.mazlov.enums;

public class Main {
    public static void main(String[] args) {
        data("MaRS");
    }

    public static void data(String name){
        for (Planets planets : Planets.values()) {
            if (planets.name().equalsIgnoreCase(name)){
                System.out.println(planets.getDescription());
            }
        }
    }
}
