package org.hillel.mazlov.enums;

public class StatusExampleFirst {
    public enum Status {
        STATUS_OPEN(0),
        STATUS_STARTED(1),
        STATUS_INPROGRESS(2),
        STATUS_ONHOLD(3),
        STATUS_COMPLETED(4),
        STATUS_CLOSED5(5);

        private final int status;

        Status(int aStatus) {
            this.status = aStatus;
        }

        public int status() {
            return this.status;
        }
    }

    public static void main(String[] args) {
        for (Status st: Status.values()){
            System.out.println(st + " values is " + st.status());
        }
    }
}
