package org.hillel.mazlov.enums;

public class ReqStatus {
    public enum Status {
        STATUS_OPEN,
        STATUS_STARTED,
        STATUS_INPROGRESS,
        STATUS_ONHOLD,
        STATUS_COMPLETED,
        STATUS_CLOSED5
    }

    public static void main(String[] args) {
        for (Status s : Status.values()) {
            System.out.println(s);
        }
    }
}
