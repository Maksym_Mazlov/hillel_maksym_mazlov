package org.hillel.mazlov.enums;

public enum Planets {
    MERCURY("HELLO FROM MERCURY"),
    VENUS("HELLO FROM VENUS"),
    EARTH("HELLO FROM EARTH"),
    MARS("HELLO FROM MARS"),
    JUPITER("HELLO FROM JUPITER"),
    SATURN("HELLO FROM SATURN"),
    URANUS("HELLO FROM URANUS"),
    NEPTUNE("HELLO FROM NEPTUNE");

    private final String description;

    Planets(String desc) {
        this.description = desc;

    }

    public String getDescription() {
        return this.description;
    }

}




