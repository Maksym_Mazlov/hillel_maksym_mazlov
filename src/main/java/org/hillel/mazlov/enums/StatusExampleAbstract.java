package org.hillel.mazlov.enums;

public class StatusExampleAbstract {

    public enum Status {

        STATUS_OPEN {
            public String description() {
                return "open";
            }
        },
        STATUS_STARTED {
            public String description() {
                return "started";
            }
        },
        STATUS_INPROGRESS {
            public String description() {
                return "inprogress";
            }
        },
        STATUS_ONHOLD {
            public String description() {
                return "onhold";
            }
        },
        STATUS_COMPLETED {
            public String description() {
                return "completed";
            }
        },
        STATUS_CLOSED5 {
            public String description() {
                return "closed";
            }
        };

        Status() {

        }

        public abstract String description();
    }

    public static void main(String[] args) {
        for (Status status : Status.values()) {
            System.out.println(status + " desc is " + status.description());
        }
    }
}
