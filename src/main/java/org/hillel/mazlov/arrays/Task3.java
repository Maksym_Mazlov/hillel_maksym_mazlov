package org.hillel.mazlov.arrays;

import java.util.Arrays;
import java.util.Random;

/*
1) Cоздать двумерный массив из 7 строк по 4 столбца в каждой из случайных целых чисел из отрезка [-5;5].
   Вывести массив на экран.
2) Создать метод, который будет сортировать указанный массив по возрастанию любым известным вам способом.
3) Проверте эти два метода в тестах
4) Сделайте меня алмином у себя на репозитории, сделайте пуш на мастер и киньте мне ссылку
*/

/**
 * HomeWork 2017.06.16
 */
public class Task3 {

    protected Task3() {
    }

    /**
     * Massive first dim
     */
    public static final int FIRST_MAS_SIZE_INDEX = 7;
    /**
     * Massive second dim
     */
    public static final int SECOND_MAS_SIZE_INDEX = 4;
    /**
     * Random limit min
     */
    public static final int RANDOM_MIN = 5;
    /**
     * Random limit max
     */
    public static final int RANDOM_MAX = 11;

    /**
     * Start
     *
     * @param args
     */
    public static void main(String[] args) {
        int[][] masResult = masiv();

        printMasiv(masResult);

        sortMasiv(masResult);

        printMasiv(masResult);

    }

    /**
     * @return create massive and return
     */
    public static int[][] masiv() {
        int[][] mas = new int[FIRST_MAS_SIZE_INDEX][SECOND_MAS_SIZE_INDEX];
        Random random = new Random();

        for (int i = 0; i < mas.length; i++) {
            for (int j = 0; j < mas[i].length; j++) {
                mas[i][j] = random.nextInt(RANDOM_MAX) - RANDOM_MIN;
            }
        }

        return mas;
    }

    /**
     * print massive to console
     *
     * @param mas to print
     */
    public static void printMasiv(int[][] mas) {

        for (int i = 0; i < mas.length; i++) {
            for (int j = 0; j < mas[i].length; j++) {
                System.out.print(String.format("%4d", mas[i][j]));
            }
            System.out.println();
        }
        System.out.println();
    }

    /**
     * @param mas to sort
     * @return sorted massive
     */
    public static int[][] sortMasiv(int[][] mas) {

        for (int i = 0; i < mas.length; i++) {
            Arrays.sort(mas[i]);
        }

        return mas;
    }
}

