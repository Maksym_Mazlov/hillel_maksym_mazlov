package org.hillel.mazlov.arrays;

/*ЗАДАЧА НОМЕР 2
    Задача перебрать двумерный массив в обратном порядке и умножить все парные элементы,
    результат вывести на экран.
*/

import java.util.Random;

/**
 * Task 2
 */
public class Task2 {

    protected Task2() {
    }

    /**
     * Massive first dim
     */
    public static final int FIRST_MAS_SIZE_INDEX = 3;
    /**
     * Massive second dim
     */
    public static final int SECOND_MAS_SIZE_INDEX = 3;
    /**
     * Random limit
     */
    public static final int RANDOM_UPPER_LIMIT = 100;

    /**
     * @param args
     */
    public static void main(String[] args) {

        int[][] mas = new int[FIRST_MAS_SIZE_INDEX][SECOND_MAS_SIZE_INDEX];
        int temp = 1;

        Random random = new Random();

        for (int i = mas.length - 1; i >= 0; i--) {
            for (int j = mas.length - 1; j >= 0; j--) {
                mas[i][j] = random.nextInt(RANDOM_UPPER_LIMIT);

                if (mas[i][j] % 2 == 0) {
                    temp *= mas[i][j];
                }
            }
        }

        System.out.println(temp);
    }
}