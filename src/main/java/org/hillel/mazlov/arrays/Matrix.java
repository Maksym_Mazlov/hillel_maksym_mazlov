package org.hillel.mazlov.arrays;

/**
 * @Max
 */
public class Matrix {

    protected Matrix() {

    }

    /**
     * matrix
     *
     * @param first
     * @param second
     * @return
     */
    public static Integer[][] multiplicar(Integer[][] first, Integer[][] second) {
        int firstRows = first.length;
        int firstColums = first[0].length;
        int secondRows = second.length;
        int secondColums = second[0].length;

        if (firstColums != secondRows) {
            throw new IllegalArgumentException("First Colums:" + firstColums
                    + "did not match Second Rows " + secondRows + ".");
        }

        Integer[][] result = new Integer[firstColums][secondColums];
        for (int i = 0; i < first.length; i++) {
            for (int j = 0; j < second.length; j++) {
                result[i][j] = 0;
            }

        }

        for (int i = 0; i < firstColums; i++) {
            for (int j = 0; j < secondColums; j++) {
                for (int k = 0; k < result.length; k++) {
                    result[i][j] += first[i][k] * second[k][j];
                }
            }
        }
        return result;
    }
}
