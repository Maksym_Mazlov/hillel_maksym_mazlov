package org.hillel.mazlov.arrays;
/*
ЗАДАЧА НОМЕР 1
    У нас есть два одномерных массива, в первом 10 элементов, а во втором 5.
    С первого получить результат умножения всех непарных элементов массива, а со второго сумму всех парных элементов.
    И в итоге вывести на экран число которое равно первого числа в степени второго числа.
*/

/**
 * Task 2
 */
public class Task1 {

    protected Task1() {

    }

    /**
     * massive size
     */
    public static final int FIRST_MAS_SIZE = 10;
    /**
     * massive size
     */
    public static final int SECOND_MAS_SIZE = 5;
    /**
     * random limit
     */
    public static final int RANDOM_UPPER_LIMIT = 100;

    /**
     * @param args
     */
    public static void main(String[] args) {

        int[] masFirst = new int[FIRST_MAS_SIZE];
        int[] masSecond = new int[SECOND_MAS_SIZE];

        int tmpFirst = 1;
        int tmpSecond = 0;

        for (int i = 0; i < masFirst.length; i++) {
            masFirst[i] = (int) (Math.random() * RANDOM_UPPER_LIMIT);
            if (masFirst[i] % 2 != 0) {
                tmpFirst *= masFirst[i];
            }
        }

        for (int i = 0; i < masSecond.length; i++) {
            masSecond[i] = (int) (Math.random() * RANDOM_UPPER_LIMIT);
            if (masSecond[i] % 2 == 0) {
                tmpSecond += masSecond[i];
            }
        }

        System.out.println(tmpFirst + " " + tmpSecond);
        System.out.println((int) Math.pow(tmpFirst, tmpSecond));
    }
}

