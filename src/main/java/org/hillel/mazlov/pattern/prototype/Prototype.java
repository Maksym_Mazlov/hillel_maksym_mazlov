package org.hillel.mazlov.pattern.prototype;

public interface Prototype {
    Prototype doClone();
}
