package org.hillel.mazlov.pattern.prototype;

public class Person implements Prototype {
    String name;

    public Person(String sound) {
        this.name = sound;
    }

    @Override
    public Prototype doClone() {
        return new Person(name);
    }

    @Override
    public String toString() {
        return "This person is named" + name;
    }
}
