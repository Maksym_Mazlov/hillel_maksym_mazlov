package org.hillel.mazlov.pattern.abstractfactory;

import org.hillel.mazlov.pattern.factory.Animal;
import org.hillel.mazlov.pattern.factory.Cat;
import org.hillel.mazlov.pattern.factory.Dog;

public class MammalFactory extends SpeciesFactory {

    public Animal getAnimal(String type) {
        if ("dog".equals(type)) {
            return new Dog();
        } else return new Cat();
    }

}
