package org.hillel.mazlov.pattern.abstractfactory;

import org.hillel.mazlov.pattern.factory.Animal;

public abstract class SpeciesFactory {
    public abstract Animal getAnimal(String type);
}
