package org.hillel.mazlov.pattern.abstractfactory;

import org.hillel.mazlov.pattern.factory.Animal;
import org.hillel.mazlov.pattern.factory.Snake;
import org.hillel.mazlov.pattern.factory.Tyrannosauurus;


public class ReptileFactory extends SpeciesFactory {
    public Animal getAnimal(String type) {
        if ("snake".equals(type)) {
            return new Snake();
        } else return new Tyrannosauurus();
    }
}
