package org.hillel.mazlov.pattern.factory;

public abstract class Animal {
    public abstract String makeSound();
}
