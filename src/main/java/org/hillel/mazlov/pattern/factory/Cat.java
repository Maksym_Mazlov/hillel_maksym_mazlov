package org.hillel.mazlov.pattern.factory;

public class Cat extends Animal {
    @Override
    public String makeSound() {
        return "Meow!";
    }
}

