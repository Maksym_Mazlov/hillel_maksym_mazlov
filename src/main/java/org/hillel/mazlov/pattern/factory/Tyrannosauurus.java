package org.hillel.mazlov.pattern.factory;

public class Tyrannosauurus extends Animal {
    @Override
    public String makeSound() {
        return "Roar!";
    }

}
