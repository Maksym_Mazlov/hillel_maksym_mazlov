package org.hillel.mazlov.pattern.factory;

public class Snake extends Animal {
    @Override
    public String makeSound() {
        return "Hiss!";
    }
}
