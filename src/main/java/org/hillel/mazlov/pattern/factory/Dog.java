package org.hillel.mazlov.pattern.factory;

public class Dog extends Animal {
    @Override
    public String makeSound() {
        return "Woof!";
    }
}
