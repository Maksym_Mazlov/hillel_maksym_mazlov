package org.hillel.mazlov.interfaces;

public interface SoundRegulator {
    void up();
    void down();
}