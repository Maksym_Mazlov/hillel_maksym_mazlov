package org.hillel.mazlov.interfaces;

import java.util.Comparator;
import java.util.Date;

public class Employee implements Comparable<Employee> {

    private int id;
    private String name;
    private int salary;
    private int age;
    private Date dateofJoining;

    public static final Comparator<Employee> ageComparator = new Comparator<Employee>() {
        @Override
        public int compare(Employee o1, Employee o2) {
            return o1.age - o2.age;
        }
    };

    public static final Comparator<Employee> salaryComparator = new Comparator<Employee>() {
        @Override
        public int compare(Employee o1, Employee o2) {
            return o1.salary - o2.salary;
        }
    };

    public static final Comparator<Employee> nameComparator = new Comparator<Employee>() {
        @Override
        public int compare(Employee o1, Employee o2) {
            return o1.name.compareTo(o2.name);
        }
    };

    public static final Comparator<Employee> dateofJoiningComparator = new Comparator<Employee>() {
        @Override
        public int compare(Employee o1, Employee o2) {
            return o1.dateofJoining.compareTo(o2.dateofJoining);
        }
    };

    protected Employee(int id, String name, int salary, int age, Date dateofJoining) {
        this.id = id;
        this.name = name;
        this.salary = salary;
        this.age = age;
        this.dateofJoining = dateofJoining;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", salary=" + salary +
                ", age=" + age +
                ", dateofJoining=" + dateofJoining +
                '}';
    }

    @Override
    public int compareTo(Employee o) {
        return this.id - o.id;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 47 * hash + this.id;
        hash = 47 * hash + (this.name != null ? this.name.hashCode() : 0);
        hash = 47 * hash + this.salary;
        hash = 47 * hash + this.age;
        hash = 47 * hash + (this.dateofJoining != null ? this.dateofJoining.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }

        if (this.getClass() != obj.getClass()) {
            return false;
        }

        final Employee other = (Employee) obj;

        if (this.id != other.id) {
            return false;
        }

        if ((this.name == null) ? (other.name != null) : !this.name.equals(other.name)) {
            return false;
        }

        if (this.salary != other.salary) {
            return false;
        }

        if (this.age != other.age) {
            return false;
        }

        if (this.dateofJoining != other.dateofJoining && (this.dateofJoining == null || !this.dateofJoining.equals(other.dateofJoining))) {
            return false;
        }
        return true;
    }
}