package org.hillel.mazlov.interfaces;

public interface ButtonSwitch {
    void switchOn();
    void switchOff();
}