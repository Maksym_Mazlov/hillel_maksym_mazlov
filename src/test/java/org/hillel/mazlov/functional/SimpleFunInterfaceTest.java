package org.hillel.mazlov.functional;


import org.junit.Test;
import sun.print.resources.serviceui_de;

import java.lang.reflect.Array;
import java.util.*;
import java.util.function.*;

public class SimpleFunInterfaceTest {
    @Test
    public void test() {
        checkWork(new SimpleFuncInterface() {
            @Override
            public void doWork() {
                System.out.println("Do work in SimpleFun iml...");
            }
        });

        checkWork(() -> System.out.println("Do work in lambda iml..."));
    }

    public static void checkWork(SimpleFuncInterface simpleFuncInterface) {
        simpleFuncInterface.doWork();
    }

    public static void eval(List<Integer> list, Predicate<Integer> predicate) {
        for (Integer n : list) {
            if (predicate.test(n)) {
                System.out.println(n + " ");
            }
        }
    }

    @Test
    public void predicateTest() {
        Predicate<Integer> isPositive = x -> x > 0;
        System.out.println(isPositive.test(5));//true
        System.out.println(isPositive.test(-7));//false

        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9);
        System.out.println("Print all numbers: ");

        //pass n as parameter
        eval(list, n -> true);

        System.out.println("Print evan numbers: ");
        eval(list, n -> n % 2 == 0);

        System.out.println("Print numbers greater than 3: ");
        eval(list, n -> n > 3);
    }

    @Test
    public void binaryOperator() {
        BinaryOperator<Integer> mul = (x, y) -> x * y;
        System.out.println(mul.apply(3, 5));//15
        System.out.println(mul.apply(10, -2));//15
    }

    @Test
    public void unaryOperator() {
        UnaryOperator<Integer> square = x -> x * x;
        System.out.println(square.apply(2));
    }

    @Test
    public void functional() {
        Function<Integer, String> convert = x -> String.valueOf(x) + " euro";
        System.out.println(convert.apply(5));
    }

    @Test
    public void consumer() {
        Consumer<Integer> printer = x -> {
            Integer integer = new Integer(x + 5);
            System.out.println(integer);
            System.out.printf("%d euro \n", x);
        };
        printer.accept(600);
    }

    @Test
    public void java8ForEachAndMap() {
        Map<String, Integer> items = new HashMap<>();
        items.put("A", 10);
        items.put("B", 20);
        items.put("C", 30);
        items.put("D", 40);
        items.put("E", 50);
        items.put("F", 60);

        //  /regular

        for (Map.Entry<String, Integer> entry : items.entrySet()) {
            System.out.println("Item " + entry.getKey() + " Count : " + entry.getValue());
        }
        System.out.println("----------------------");

        //java 8
        items.forEach((k, v) -> System.out.println("Item: " + k + "Count: " + v));

        items.forEach((k, v) -> {
            System.out.println(" Item: " + k + "Count: " + v);
            if ("E".equals(k)) {
                System.out.println("Hello E");
            }
        });
    }

    @Test
    public void java8ForEachAndList() {
        List<String> items = new ArrayList<>();
        items.add("A");
        items.add("B");
        items.add("C");
        items.add("D");
        items.add("E");

        //regular
        for (String item : items) {
            System.out.println(item);
        }

        //java 8
        items.forEach(item -> System.out.println(item));
        System.out.println("=========================================");

        items.forEach(System.out::println); //ссылка на метод
        System.out.println("---------------------------------------");
        //Stream and filter
        items.stream().filter(s -> s.contains("B")).forEach(System.out::println);
    }

    @Test
    public void testTask(){
        Supplier<List<Dog>> supplier = () ->{

            List<Dog> list = new ArrayList<>();
            list.add( new Dog("Sfff",4444) );
            list.add( new Dog("Sfff",4444));
            list.add( new Dog("Sfff",4444));
            list.add( new Dog("Sfff",4444));
            return list;
        };

//      supplier.get().forEach((dog) -> System.out.println(dog));
        supplier.get().forEach(System.out::println);

    }

}
