package org.hillel.mazlov.interfaces;

import org.junit.Test;

import java.util.Arrays;

public class SortFruitObjectTest {
    @Test
    public void SortFruitObjects() {
        Fruit[] fruits = new Fruit[5];

        Fruit pineapple = new Fruit("Peneapple", "Peneapple des", 70);
        Fruit aplle = new Fruit("Aplle", "Aplle des", 100);
        Fruit orange = new Fruit("Orange", "Orange des", 80);
        Fruit banan = new Fruit("Banan", "Banan des", 70);
        Fruit aplle2 = new Fruit("Aplle", "Aplle des", 110);

        fruits[0] = pineapple;
        fruits[1] = aplle;
        fruits[2] = orange;
        fruits[3] = banan;
        fruits[4] = aplle2;

        Arrays.sort(fruits, Fruit.fruitNameComparator);
        int i = 0;
        for (Fruit tmp : fruits) {
            System.out.println("fruits " + ++i + " : " + tmp.getFruitName()
                    + ", Quantity : " + tmp.getQuantity());
        }
    }
}