package org.hillel.mazlov.interfaces;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class SortAnArrayTest {
    @Test
    public void sortAnArray() {
        String[] fruits = {"Peneapple", "Apple", "Orange", "Banana"};
        Arrays.sort(fruits);
        int i = 0;
        for (String tmp : fruits) {
            System.out.println("fruits " + ++i + " : " + tmp);
        }
    }

    @Test
    public void sortAnArrayList() {
        List<String> fruits = new ArrayList<>();
        fruits.add("Peneapple");
        fruits.add("Apple");
        fruits.add("Orange");
        fruits.add("Banana");

        Collections.sort(fruits);
        int i = 0;
        for (String tmp : fruits) {
            System.out.println("fruits " + ++i + " : " + tmp);
        }
    }
}