package org.hillel.mazlov.nio;

import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.FileAttribute;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.security.Permission;
import java.security.Permissions;
import java.util.Set;

public class NioTest {
    @Test
    public void copy() {
        Path pathSource = Paths.get("/home/maksym/Desktop/Dev/Hillel/src/main/resources/testing/", "test.txt");

        File destFile = new File("/home/maksym/Desktop/Dev/Hillel/src/main/resources/testing/newTest1.txt");
        File sourceFile = new File("/home/maksym/Desktop/Dev/Hillel/src/main/resources/testing/newTest1.txt");

        try (FileOutputStream fos = new FileOutputStream(destFile);
             FileInputStream fis = new FileInputStream(sourceFile)) {

            long noOfBytes = Files.copy(pathSource, fos);
            System.out.println(noOfBytes);

            Path destPath1 = Paths.get("/home/maksym/Desktop/Dev/Hillel/src/main/resources/testing/", "newTest2.txt");
            noOfBytes = Files.copy(fis, destPath1, StandardCopyOption.REPLACE_EXISTING);
            System.out.println(noOfBytes);

            Path destPath2 = Paths.get("/home/maksym/Desktop/Dev/Hillel/src/main/resources/testing/", "newTest3.txt");
            Path target = Files.copy(pathSource, destPath2, StandardCopyOption.REPLACE_EXISTING);
            System.out.println(target.getFileName());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void directory() throws Exception {
        Set<PosixFilePermission> perm = PosixFilePermissions.fromString("rwxrwx--x");
        FileAttribute<Set<PosixFilePermission>> fileAttribute = PosixFilePermissions.asFileAttribute(perm);
        Path path = Paths.get("/home/maksym/Desktop/Dev/Hillel/src/main/resources/testing/", "Parent", "Child");

        Files.createDirectories(path, fileAttribute);
        Files.createTempDirectory(path, "Concretepage");

    }

    @Test
    public void file() throws IOException {
        Set<PosixFilePermission> permissions = PosixFilePermissions.fromString("rwxrwx--x");
        FileAttribute<Set<PosixFilePermission>> fileAttribute = PosixFilePermissions.asFileAttribute(permissions);
        Path path = Paths.get("/home/maksym/Desktop/Dev/Hillel/src/main/resources/", "testing", "test222.txt");
        Files.createFile(path, fileAttribute);
    }
}
