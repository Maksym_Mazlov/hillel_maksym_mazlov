package org.hillel.mazlov.nio;

import org.junit.Test;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class TestReadFile {
    @Test
    public void testReader() {
        String fileName = "/home/maksym/Desktop/Dev/Hillel/pom.xml";
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
//       Еще один вариант вывода потока
//            stream.forEach(x->{
//                System.out.println(x);
//            });

            stream.forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testSecondReader() {
        String fileNmame = "/home/maksym/Desktop/Dev/Hillel/pom.xml";
        List<String> list = new ArrayList<>();
        try (Stream<String> stream = Files.lines(Paths.get(fileNmame))) {
            list = stream
                    .filter(line -> line.startsWith("<"))
                    .map(String::toUpperCase)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        list.forEach(System.out::println);
    }

    @Test
    public void testReference() {
        ArrayList<Integer> arrayList = new ArrayList();
        arrayList.add(1);
        arrayList.add(1);
        arrayList.add(1);
        arrayList.add(1);
        arrayList.add(1);
        arrayList.add(1);
        arrayList.add(1);
        arrayList.add(1);
        arrayList.add(1);
        // каждый едемент arrayList кидаем в метод square
        arrayList.forEach(TestReadFile::square);
    }

    public static void square(int num) {
        System.out.println(Math.pow(num, 2));
    }

    @Test
    public void paths() throws IOException {
        Path path = Paths.get("/home/maksym/Desktop/Dev/Hillel/src/main/resources/testing/test.txt");
        String question = "To be or not to be?";
        Files.write(path, question.getBytes());

        Path path2 = Paths.get("/home/maksym/Desktop/Dev/Hillel/src/main/resources/testing/test.txt");
        System.out.println(path2.getFileName() + " " + path2.getName(0) + " "
                + path2.getNameCount() + " " + path2.subpath(0, 2) + " "
                + path2.getParent() + " " + path2.getRoot());
        TestReadFile.convertPaths();
    }

    private static void convertPaths() {
        Path relative = Paths.get("test.txt");
        System.out.println("Rel path: " + relative);
        Path absolut = relative.toAbsolutePath();
        System.out.println("Absolut path: " + absolut);
    }

    @Test
    public void copy() {
        Path oldFile = Paths.get("/home/maksym/Desktop/Dev/Hillel/src/main/resources/testing/", "test.txt");
        Path newFile = Paths.get("/home/maksym/Desktop/Dev/Hillel/src/main/resources/testing2/", "newTest.txt");
        try (OutputStream outputStream = new FileOutputStream(newFile.toFile())){
            Files.copy(oldFile,outputStream);
        }
        catch (IOException ex){
            ex.printStackTrace();
        }
    }
}
