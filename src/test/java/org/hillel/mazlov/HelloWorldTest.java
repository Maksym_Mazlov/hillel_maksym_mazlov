package org.hillel.mazlov;

import org.junit.Test;

import static org.hillel.mazlov.HelloWorld.print;
import static org.hillel.mazlov.HelloWorld.println;

public class HelloWorldTest {
    @Test
    public void helloWorldTest() {
        String string = "Java";
        Integer integer = 50;
        println(string);
        println();
        print(integer);
    }
}