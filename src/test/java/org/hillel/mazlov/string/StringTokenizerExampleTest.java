package org.hillel.mazlov.string;

import org.junit.Test;

public class StringTokenizerExampleTest {
    @Test
    public void splitBySpaceTest() throws Exception {
        String str = "Hello World, Ok";

        StringTokenizerExample stringTokenizerExample = new StringTokenizerExample();
        stringTokenizerExample.splitBySpace(str);
    }
    @Test
    public void splitByCommaTest() throws Exception {
        String str = "Hello World, Ok";

        StringTokenizerExample stringTokenizerExample = new StringTokenizerExample();
        stringTokenizerExample.splitByComma(str);
    }
    @Test
    public void readCsvTest(){
        String path = "./src/main/resources/files_data.csv";
        ReadFile.readCsv(path);
    }
}