package org.hillel.mazlov.regex;

import org.junit.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class UsernameValidatorTest {
    private UsernameValidator usernameValidator;

    @BeforeTest
    public void initData() {
        usernameValidator = new UsernameValidator();
    }

    @DataProvider
    public Object[][] ValidUsernameProvider() {
        return new Object[][]{{new String[]{"alex34", "alex_2002", "alex3-4", "alex3-4_good"}}
        };
    }

    @DataProvider
    public Object[][] InvalidUsernameProvider() {
        return new Object[][]{{new String[]{"alex", "al@ekseq", "alec1234567855975587"}}
        };
    }

    @Test(dataProvider = "ValidUsernameProvider")
    public void ValidUsernameProviderTest(String[] Username) {
        for (String temp : Username) {
            boolean valid = usernameValidator.validate(temp);
            System.out.println("Username is valid : " + temp + " , " + valid);
            Assert.assertEquals(true, valid);
        }
    }

//    @Test(dataProvider = "InvalidUsernameProvider", dependsOnMethods = "InvalidUsernameTest")
//    public void InvalidUsernameTest(String[] Username){
//        for (String temp : Username) {
//            boolean valid = usernameValidator.validate(temp);
//            System.out.println("Username is valid : " + temp + " , " + valid);
//            Assert.assertEquals(false, valid);
//        }
//    }

}
