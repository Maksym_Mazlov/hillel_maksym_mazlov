package org.hillel.mazlov.reflection;

import org.hillel.mazlov.annotations.People;
import org.junit.Test;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Arrays;

public class Reflection {
    @Test
    public void reflection() {
        People people = new People();
        Class myClass = people.getClass();

        Class mySecondClass = People.class;

        System.out.println(Arrays.toString(people.getClass().getFields()));
        System.out.println(people.getClass().getModifiers());
        System.out.println(Arrays.toString(people.getClass().getMethods()));

        Package pack = people.getClass().getPackage();
        System.out.println(pack.getName());

        Class[] interfaces = myClass.getInterfaces();
        for (int i = 0; i < interfaces.length; i++) {
            System.out.print(i == 0 ? "implemrnts " : ", ");
            System.out.print(interfaces[i].getSimpleName());
        }
        System.out.println(" {");
        Field[] fields = myClass.getDeclaredFields();
        for (Field field :
                fields) {
            System.out.println("\t" + field.getModifiers() + " "
                    + field.getType() + " " + field.getName() + ";");
        }

        Method[] methods = myClass.getDeclaredMethods();
        for (Method m : methods) {
            Annotation[] annotation = m.getAnnotations();
            System.out.print("\t");

            for (Annotation a : annotation) {
                System.out.print("@" + a.annotationType().getSimpleName() + " ");
            }
                System.out.println();
                System.out.print("\t" + m.getModifiers() + " " + m.getReturnType() + " " + m.getName() + " ");
            }
        }



    }

