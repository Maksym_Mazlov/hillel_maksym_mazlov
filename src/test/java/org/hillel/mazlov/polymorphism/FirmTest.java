package org.hillel.mazlov.polymorphism;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;

public class FirmTest {
    @Test
    public void poly() {
        Staff personnel = new Staff();
        personnel.payday();
    }

    @Test
    public void sortName() {
        Staff personnel = new Staff();
        System.out.println("-= Before sorting by name =-");
        for (StaffMember member : personnel.staffList) {
            System.out.println(member.name);
        }

        Collections.sort(personnel.staffList, StaffMember.nameComparator);

        System.out.println("-= After sorting by name =-");
        for (StaffMember member : personnel.staffList) {
            System.out.println(member.name);
        }
    }

    @Test
    public void sortAddress() {
        Staff personnel = new Staff();
        System.out.println("-= Before sorting by address =-");
        for (StaffMember member : personnel.staffList) {
            System.out.println(member.address);
        }

        Collections.sort(personnel.staffList, StaffMember.addressComparator);

        System.out.println("-= After sorting by address =-");
        for (StaffMember member : personnel.staffList) {
            System.out.println(member.address);
        }
    }

    @Test
    public void sortPhone() {
        Staff personnel = new Staff();
        System.out.println("-= Before sorting by phone =-");
        for (StaffMember member : personnel.staffList) {
            System.out.println(member.phone);
        }

        Collections.sort(personnel.staffList, StaffMember.phoneComparator);

        System.out.println("-= After sorting by phone =-");
        for (StaffMember member : personnel.staffList) {
            System.out.println(member.phone);
        }
    }

    @Test
    public void sortPayRate() {
        ArrayList<Employee> list = new ArrayList<>();
        list.add(new Employee("Vasya", "023 Om Lineage", "145-155-155", "555644", 1225.55));
        list.add(new Employee("Bond", "007 J Bond", "000-000-007", "007", 1007));
        list.add(new Employee("Jura", "658 Suda Ave", "245-155-355", "11111", 555));

        System.out.println("-= Before sorting by pay =-");
        for (Employee member : list) {
            System.out.println("Employee: " + member.name + " obtain " + member.payRate);
        }

        Collections.sort(list, new EmployeePayRateComparator());

        System.out.println("-= After sorting by pay =-");
        for (Employee member : list) {
            System.out.println("Employee: " + member.name + " obtain " + member.payRate);
        }
    }
}