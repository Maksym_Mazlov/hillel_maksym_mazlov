package org.hillel.mazlov.polymorphism;


import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UserTest {
    @Test
    public void setPasswordNormal() {
        User user = new User();
        user.setPassword("qwerty");
        assertEquals("qwerty", user.getPassword());
    }

    @Test
    public void setPasswordShort() {
        User user = new User();
        user.setPassword("aa");
        assertEquals(null, user.getPassword());
    }

    @Test(expected = NullPointerException.class) // В тесте мы ожидаем NullPointerException!
    public void setPasswordNull() {
        User user = new User();
        user.setPassword(null);
        assertEquals(null, user.getPassword());
    }

    @Test
    public void setLoginValid() {
        User user = new User();
        user.setLoginId("a123");
        assertEquals("a123", user.getLoginId());
    }

    @Test
    public void setLoginInValid() {
        User user = new User();
        user.setLoginId("##%#&*");
        assertEquals(null, user.getLoginId());
    }
}