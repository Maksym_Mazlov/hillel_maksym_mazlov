package org.hillel.mazlov.reports;

import net.sf.dynamicreports.jasper.builder.JasperReportBuilder;
import net.sf.dynamicreports.report.exception.DRException;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class Reports {
    @Test
    public void test() {
        InvoiceData data = new InvoiceData();
        InvoiceDesign design = new InvoiceDesign(data);
        try {
            JasperReportBuilder report = design.build();
            report.toPdf(new FileOutputStream(new File("/home/maksym/Desktop/Dev/Hillel/reports.pdf")));
        } catch (DRException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}