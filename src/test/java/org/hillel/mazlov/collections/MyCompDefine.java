package org.hillel.mazlov.collections;

import org.junit.Test;

import java.util.TreeSet;

public class MyCompDefine {
    @Test
    public void test() {
        TreeSet<Empl> nameComp = new TreeSet<>(new MyNameComp());
        nameComp.add(new Empl("Ram", 3000));
        nameComp.add(new Empl("Jolo", 8000));
        nameComp.add(new Empl("Lol", 1000));
        nameComp.add(new Empl("Gusi", 2400));

        for (Empl empl : nameComp) {
            System.out.println(empl);
        }
        System.out.println();
        nameComp.forEach(empl -> {
            System.out.println(empl);
        });
        System.out.println("**************************");

        TreeSet<Empl> salComp = new TreeSet<>(new MySalaryComp());
        salComp.add(new Empl("Ram", 3000));
        salComp.add(new Empl("Jolo", 8000));
        salComp.add(new Empl("Lol", 1000));
        salComp.add(new Empl("Gusi", 2400));
        for (Empl empl : salComp) {
            System.out.println(empl);
        }
    }
}