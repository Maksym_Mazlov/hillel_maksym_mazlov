package org.hillel.mazlov.collections;

import org.hillel.mazlov.collections.homework.MyOwnArrayListGen;
import org.junit.Test;

public class MyOwnArrayListGenTest {
    @Test
    public void test() {
        MyOwnArrayListGen fruitList = new MyOwnArrayListGen();
        fruitList.add("Mango");
        fruitList.add(5);

        for (int i = 0; i < fruitList.size(); i++) {
            System.out.println(fruitList.get(i));
        }

        MyOwnArrayListGen<String> fruitList2 = new MyOwnArrayListGen<>();
        fruitList2.add("Apple");
        fruitList2.add("Banana");
        //   fruitList2.add(5); can not add !

        System.out.println(fruitList2.get(1));;

        for (int i = 0; i < fruitList2.size(); i++) {
            System.out.println(fruitList2.get(i));
        }

        fruitList2.remove(1);

        for (int i = 0; i < fruitList2.size(); i++) {
            System.out.println(fruitList2.get(i));
        }


    }
}
