package org.hillel.mazlov.inner;

import org.junit.Test;

public class OuterTest {
    @Test
    public void outerTest() {
        //Nested класс (вложенный). Вложеный класс всегда static
        Outer.Nested nested = new Outer.Nested();
        nested.printNestedText();

        Outer outer = new Outer();
        Outer.Inner inner = outer.new Inner();
        inner.printText();
        outer.doIt();

        //Анонимный класс с переопределением метода doIt() с класса Outer
        Outer outer1 = new Outer() {
            public void doIt() {
                System.out.println("Анонимный класс - doIt()");
            }
        };
        outer1.doIt();
    }

    @Test
    public void mYIntTest() {
        final String TEXT_TO_PRINT = "Text ...";

        //Анонимный класс
        MyInterface myInterface = new MyInterface() {
            private String text;

            {
                this.text = TEXT_TO_PRINT; // Блок инициализации - инициализация "на ходу"
            }

            @Override
            public void doIt() {
                System.out.println("Anonymous class doIt");
                System.out.println(this.text);
            }
        };
        myInterface.doIt();
    }

    @Test
    public void local() {
        LocalInner localInner = new LocalInner();
        localInner.display();
    }

    @Test
    public void cacheTest() {
        String key = "Password";
        Integer pass = 556446;
        Cache cache = new Cache();
        cache.store(key, pass);
        System.out.println(cache.get("Password"));
        System.out.println(cache.getData("Password"));
    }
}