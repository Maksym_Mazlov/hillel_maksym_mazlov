package org.hillel.mazlov.inner;

import de.bechte.junit.runners.context.HierarchicalContextRunner;
import org.junit.*;
import org.junit.runner.RunWith;

@RunWith(HierarchicalContextRunner.class)
public class NestedTest {
    @BeforeClass
    public static void beforeAllTestMethods() {
        System.out.println("Invoked once before all test methods");
    }

    @Before
    public void beforeEachTestMethods() {
        System.out.println("Invoked once before each test methods");
    }

    @After
    public void afterEachTestMethods() {
        System.out.println("Invoked after each test methods");
    }

    @AfterClass
    public static void afterAllTestMethods() {
        System.out.println("Invoked once after all test methods");
    }

    @Test
    public void rootClassTest() {
        System.out.println("Root class test");
    }

    public class ContextA {
        @Before
        public void beforeEachTestMethodOfContextA() {
            System.out.println("Invoked before each test methods of context A");
        }

        @After
        public void afterEachTestMethodOfContextA() {
            System.out.println("Invoked after each test methods of context A");
        }

        @Test
        public void contextATest() {
            System.out.println("Context A test");
        }

        public class ContextC {
            @Before
            public void beforeEachTestMethodOfContextC() {
                System.out.println("Invoked before each test methods of context C");
            }

            @After
            public void afterEachTestMethodOfContextC() {
                System.out.println("Invoked after each test methods of context C");
            }

            @Test
            public void contextCTest() {
                System.out.println("Context C test");
            }
        }
    }
}