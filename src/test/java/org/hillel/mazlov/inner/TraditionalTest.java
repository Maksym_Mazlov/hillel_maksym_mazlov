package org.hillel.mazlov.inner;

import org.junit.*;

public class TraditionalTest {

    @BeforeClass
    public static void beforeAllTestMethod() {
        System.out.println("Before class");
    }

    @Before
    public  void beforeEachTestMethod() {
        System.out.println("Before");
    }

    @After
    public  void afterEachTestMethod() {
        System.out.println("After");
    }

    @AfterClass
    public static void afterAllTestMethod() {
        System.out.println("After class");
    }

    @Test
    public  void testOne() {
        System.out.println("Test One");
    }

    @Test
    public  void testTwo() {
        System.out.println("Test Two");
    }
}