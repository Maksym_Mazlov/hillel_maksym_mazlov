package org.hillel.mazlov.pattern.builder;


import org.hillel.mazlov.pattern.abstractfactory.AbstractFactory;
import org.hillel.mazlov.pattern.abstractfactory.SpeciesFactory;
import org.hillel.mazlov.pattern.factory.Animal;
import org.hillel.mazlov.pattern.factory.AnimalFactory;
import org.hillel.mazlov.pattern.prototype.Person;
import org.hillel.mazlov.pattern.singleton.SingletonExample;
import org.junit.Test;

import java.util.Arrays;


public class TestPatterns {
    @Test
    public void builder() {
        Student student = new Student.Builder().name("Max").age(25).language(Arrays.asList("Chinese", "english")).build();
        System.out.println(student);
    }

    @Test
    public void factory() {

        Animal a1 = AnimalFactory.getAnimal("feline");
        System.out.println("a1 sound: " + a1.makeSound());

        Animal a2 = AnimalFactory.getAnimal("canine");
        System.out.println("a2 sound: " + a2.makeSound());
    }

    @Test
    public void abstractFactory() {
        AbstractFactory abstractFactory = new AbstractFactory();
        SpeciesFactory speciesFactory1 = abstractFactory.getSpeciesFactory("reptile");

        Animal animal1 = speciesFactory1.getAnimal("tyran");
        System.out.println("animal1 sound: " + animal1.makeSound());

        Animal animal2 = speciesFactory1.getAnimal("snake");
        System.out.println("animal2 sound: " + animal2.makeSound());

        SpeciesFactory speciesFactory2 = abstractFactory.getSpeciesFactory("mammal");


        Animal animal3 = speciesFactory2.getAnimal("dog");
        System.out.println("animal3 sound: " + animal3.makeSound());

        Animal animal4 = speciesFactory2.getAnimal("cat");
        System.out.println("animal4 sound: " + animal4.makeSound());

        Animal animal = new AbstractFactory().getSpeciesFactory("mammal").getAnimal("dog");
        System.out.println("animal sound: " + animal.makeSound());
    }

    @Test
    public void prototype() {
        Person person1 = new Person("Fred");
        System.out.println("person 1: " + person1);

        Person person2 = (Person) person1.doClone();
        System.out.println("person 2: " + person2);
    }

    @Test
    public void singleton() {
        SingletonExample singletonExample = SingletonExample.getInstance();
        singletonExample.sayHello();

        SingletonExample singletonExample2 = SingletonExample.getInstance();
        singletonExample.sayHello();

        SingletonExample singletonExample3 = SingletonExample.getInstance();
        singletonExample.sayHello();
    }
}
