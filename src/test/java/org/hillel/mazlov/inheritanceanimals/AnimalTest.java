package org.hillel.mazlov.inheritanceanimals;

import org.hillel.mazlov.inheritancebirds.Pigeon;
import org.junit.Test;

public class AnimalTest {
    @Test
    public void checkOurAnimalsTest() {
        Terier terier = new Terier();
        System.out.println(Terier.BREEZE);
        System.out.println(terier.whatDoesDogSay);
        System.out.println(terier.whatDoesDogEat);
        terier.setAge(12);
        terier.setName("Rex");
        System.out.println(terier.getName());
        System.out.println(terier.getAge());

        System.out.println(terier.animalLegs());

        System.out.println("---------------------------");

        Sphinx sphinx = new Sphinx();
        System.out.println(Sphinx.BREEZE);
        System.out.println(sphinx.whatDoesCatSay);
        System.out.println(sphinx.whatDoesCatEat);
        sphinx.setAge(12);
        sphinx.setName("Murchick");
        System.out.println(sphinx.getName());
        System.out.println(sphinx.getAge());

        System.out.println(sphinx.animalLegs());

        System.out.println("---------------------------");

        Pigeon pigeon = new Pigeon();
        System.out.println(pigeon.animalLegs());
        System.out.println(pigeon.getBirdsSay());
    }
}