package org.hillel.mazlov.junit;

import org.junit.jupiter.api.*;
import java.util.function.Supplier;

public class AppTest {

    @BeforeAll
    static void setup() {
        System.out.println("@BeforAll executed");
    }

    @BeforeEach
    void setupThis() {
        System.out.println("@BeforForEach executed");
    }

    @Tag("DEV")
    @Test
    void testCalcOne() {
        System.out.println("=======TEST ONE EXECUTED=======");
        Assertions.assertEquals(4, Calculator.sum(2, 2));
    }

    @Tag("PROD")
    @Disabled
    @Test
    void testCalcTWO() {
        System.out.println("=======TEST TWO EXECUTED=======");
        Assertions.assertEquals(6, Calculator.sum(2, 4));
    }

    @AfterEach
    void tearThis() {
        System.out.println("@AfterEach executed");
    }

    @AfterAll
    static void tear() {
        System.out.println("@AfterAll executed");
    }

    @Tag("DEV")
    @Test
    void testCase() {
        //Test will pass
        Assertions.assertNotEquals(3, Calculator.sum(2, 2));
        //Test will fail
        Assertions.assertNotEquals(5, Calculator.sum(2, 2), "CalculatorTest.sum(2,2)test failed");
        //Test will fail
        Supplier<String> messageSupplier = () -> "CalculatorTest.sum(2,2)test failed";
        Assertions.assertNotEquals(5, Calculator.sum(2, 2), messageSupplier);
    }
}