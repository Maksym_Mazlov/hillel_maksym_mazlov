package org.hillel.mazlov.junit;

import org.junit.Test;
import static org.junit.Assert.*;

public class CalculatorTest {
    @Test
    public void testFindMax(){
        assertEquals(4,Calculator.findMax(new int[]{1,2,4,2}));
        assertEquals(-1,Calculator.findMax(new int[]{-12,-1,-3,-2}));
    }

    @Test
    public void multiplicationOdZeroIntegersShouldReturnZero(){
        Calculator calculator = new Calculator();
        //assert statements
        assertEquals("10 x 0 must be 1", 0,calculator.multiply(10,0));
        assertEquals("0 x 10 must be 0", 0,calculator.multiply(0,10));
        assertEquals("0 x 0 must be 0", 0,calculator.multiply(0,0));
    }
}