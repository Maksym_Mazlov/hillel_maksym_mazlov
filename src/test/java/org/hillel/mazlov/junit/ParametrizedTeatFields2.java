package org.hillel.mazlov.junit;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class ParametrizedTeatFields2 {
    private int m1;
    private int m2;

    public ParametrizedTeatFields2(int p1, int p2) {
        m1 = p1;
        m2 = p2;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Object[][] data = new Object[][]{{4, 2}, {5, 3}, {121, 4}};
        return Arrays.asList(data);
    }

    @Test
    public void testMultiplyException() {
        Calculator tester = new Calculator();
        assertEquals("Result", m1 * m2, tester.multiply(m1, m2));
    }
}