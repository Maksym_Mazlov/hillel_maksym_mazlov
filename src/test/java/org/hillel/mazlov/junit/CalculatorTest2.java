package org.hillel.mazlov.junit;

import org.junit.*;
import static org.junit.Assert.assertEquals;

public class CalculatorTest2 {
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        System.out.println("before class");
//        BufferedReader bufferedReader = new BufferedReader(new FileReader(new File("test.txt")));
//        bufferedReader.read();

    }

    @Before
    public void setUp() throws Exception {
        System.out.println("before");
    }

    @Test
    public void testFindMax() {
        System.out.println("test case find max");
        assertEquals(4, Calculator.findMax(new int[]{1, 3, 4, 2}));
        assertEquals(-2, Calculator.findMax(new int[]{-12, -3, -4, -2}));
    }

    @Test
    public void testCube() {
        System.out.println("testcase cube");
        assertEquals(27, Calculator.cube(3));
    }

    @Test
    public void testReverseWord() {
        System.out.println("test case reverts word");
        assertEquals("ym eman si nahk ", Calculator.reverseWord("my name is khan"));
    }

    @After
    public void tearDown() throws Exception {
        System.out.println("after");
    }

    @AfterClass
    public static void tearDownAfterClass() throws Exception {
        System.out.println("after class");
    }
}