package org.hillel.mazlov.arrays;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class Task3Test {
    @Test
    public void testMasiv() {
        int[][] masResult = Task3.masiv();
        Assert.assertEquals(7,masResult.length);
        Assert.assertEquals(4,masResult[0].length);

    }

    @Test
    public void testSortMasiv() {
        int[][] mas = {{1, 5, -3}, {3, 5, -5}};
        int[][] masSorted = {{-3, 1, 5}, {-5, 3, 5}};
        Task3.sortMasiv(mas);
        Assert.assertArrayEquals(masSorted, mas);
    }

}