package org.hillel.mazlov.arrays;

import org.junit.Test;
import java.util.Arrays;

public class MatrixTest {
    @Test
    public void matrix() {
        Integer[][] first = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        Integer[][] second = {{1, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        System.out.println(Arrays.deepToString(Matrix.multiplicar(first, second)));
    }
}
